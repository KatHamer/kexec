# KExec

Executes inputted code while seamlessly handling tracebacks and asynchronous functions.

# Why?

This code is useful for applications which require execution of user inputted code. This should be used sparingly, and only accept code from trusted users. Although the code does handle asynchronous behaviour and exceptions, it will run whatever is inputted. This can result in a huge security hole if not used correctly.

# Correct usage

I personally use this code for my Telegram Userbot. A Telegram Userbot is a piece of code that allows you to automate the popular chat platform, Telegram. This execution code has safeguards to make sure it is only run by me. It allows me to run bulk actions through my account without access to a computer, straight from a chat window.

To ensure that this module is used safely, only allow code to be executed from trusted sources. Anyone who can provide input to this code has access to the entirety of what it's running on.

